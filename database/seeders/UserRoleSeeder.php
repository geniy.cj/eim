<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRoleSeeder extends Seeder
{
    private $roles = ["Parent","Administrator","Manager","Employee"];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            UserRole::create(
                [
                    "role" => $role
                ]
            );
        }
    }
}
