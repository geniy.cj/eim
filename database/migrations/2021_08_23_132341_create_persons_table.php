<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('middle_name')->nullable();
            $table->enum('sex', ['female','male'])->default('male');
            $table->string('mobile_phone', 10)->nullable();
            $table->string('img')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('person_type', ['parent','child','employee','kinsman','nanny'])->default('parent');
            $table->bigInteger('family_id')->unsigned()->nullable();
            $table->bigInteger('groups_learners_id')->unsigned()->nullable();
            $table->time('time_begin')->default('08:00:00');
            $table->time('time_finish')->default('18:00:00');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}
