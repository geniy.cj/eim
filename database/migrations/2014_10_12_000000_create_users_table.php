<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('mobile_phone', 10)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('mobilephone_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('person_id')->unsigned()->nullable();
            $table->bigInteger('user_role_id')->unsigned()->nullable();
//            $table->enum('user_role',['Administrator','Manager','Employee','Parent'])->default('Parent');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
