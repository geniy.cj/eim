<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin_access', function ($user) {
            return (Auth::user() && Auth::user()->UserRole && Auth::user()->UserRole->role === "Administrator");

        });

        Gate::define('employee_access', function ($user) {
            return (Auth::user() && Auth::user()->UserRole && (Auth::user()->UserRole->role === "Administrator" || Auth::user()->UserRole->role  === "Employee"));
        });
    }
}
