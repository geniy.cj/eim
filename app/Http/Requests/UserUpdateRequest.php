<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "max:255",
                "min:4"
            ],
//            'email' => [
//                "max:255",
//                "email",
//                Rule::unique('users')
//                    ->ignore($this->user)
//            ],
            'mobile_phone' => [
                "required",
                "digits:10",
                Rule::unique('users')
                    ->ignore($this->user)
            ]
        ];
    }
}
