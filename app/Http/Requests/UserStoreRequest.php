<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                "max:255",
                "min:4"
            ],
//            'email' => [
//                "max:255",
//                "email",
//                "unique:users,email"
//            ],
            'mobile_phone' => [
                "required",
                "digits:10",
                "unique:users,mobile_phone"
            ],
            'password' => 'required|min:8|confirmed|regex:/(^([a-zA-Z0-9]+)$)/u'
        ];
    }
}
