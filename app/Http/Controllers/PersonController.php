<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PersonService;
use App\Services\FamilyService;
use App\Http\Requests\PersonUpdateRequest;
use App\Models\Person;
use App\Http\Requests\PersonStoreRequest;

class PersonController extends Controller
{
    public function __construct(PersonService $persons, FamilyService $family)
    {
        $this->persons = $persons;
        $this->family = $family;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'person.index',
            ["persons" => $this->persons->all(),]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'person.create',
            [
//                "family" => $this->family->all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonStoreRequest $request)
    {
        $person = $this->persons->create($request->all());
        return redirect()->route('persons.show', ["person" => $person->id])->withSuccess(
            'Person #' . $person->id . " was created!"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'person.show',
            ["person" => $this->persons->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'person.edit',
            [
                "person" => $this->persons->findById($id),
//                "family" => $this->family->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonUpdateRequest $request, Person $person)
    {
        $this->persons->update($person->id, $request->except(['_method', '_token']));
        return redirect()->route('persons.show', ["person" => $person->id])->withSuccess(
            'person #' . $person->id . " was updated!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->persons->destroy($id);
        return redirect()->route('persons.index')->withSuccess('person #' . $id . " was deleted!");
    }
}
