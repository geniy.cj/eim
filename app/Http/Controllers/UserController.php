<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Services\UserService;

class UserController extends Controller
{
    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    public function profile(Request $request)
    {
        return view(
            'user.profile',
            [
                "user" => Auth::user(),
            ]
        );
    }

    public function update(UpdateUserProfileRequest $request)
    {
        $this->user->update(
            Auth::id(),
            [
                "name" => $request->input('name'),
            ]
        );

        return redirect()->route('profile');
    }
}
