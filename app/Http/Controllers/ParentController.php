<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FamilyService;
use App\Services\ParentService;
use App\Http\Requests\PersonUpdateRequest;
use App\Models\Person;
use App\Http\Requests\PersonStoreRequest;
use Illuminate\Support\Facades\Auth;

class ParentController extends Controller
{
    public function __construct(ParentService $parents, FamilyService $family)
    {
        $this->parents = $parents;
        $this->family = $family;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'parent.index',
            ["parents" => $this->parents->getByFamily(Auth::user()),]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if (empty($user->person_id)) {
            $name = $user->name;
        }else{
            $name = "";
        }

        return view(
            'parent.create',
            [
                "name" => $name
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonStoreRequest $request)
    {
        $user = Auth::user();
        $person = $this->parents->createWithFamily($request->all(),$user);
        return redirect()->route('parents.show', ["parent"=>$person->id])->withSuccess(
           'Особу '. $person->name . " було створено!"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'parent.show',
            ["parent" => $this->parents->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'parent.edit',
            [
                "parent" => $this->parents->findById($id),
                "family" => $this->parents->getByFamily(Auth::user())
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonUpdateRequest $request, Person $parent)
    {
        $this->parents->update($parent->id, $request->except(['_method', '_token']));
        return redirect()->route('parents.show', ["parent" => $parent->id])->withSuccess(
            'Інформацію #' . $parent->id . " оновлено!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->parents->destroy($id);
        return redirect()->route('parents.index')->withSuccess('Дані про особу #' . $id . " були видалені!");
    }
}
