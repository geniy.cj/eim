<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\UserRoleService;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserStoreRequest;
use App\Models\User;

class UserManagementController extends Controller
{
    public function __construct(UserService $users, UserRoleService $userRole)
    {
        $this->users = $users;
        $this->userRole = $userRole;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Role = $this->userRole->getAdministrator()->id;
        return view(
            'management_user.index',
            ["users" => $this->users->getAllNotRole($Role)]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'management_user.create',
            [
                "user_roles" => $this->userRole->all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = $this->users->create($request->all());
        return redirect()->route('users.show', ["user" => $user->id])->withSuccess(
            'User #' . $user->id . " was created!"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'management_user.show',
            ["user" => $this->users->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'management_user.edit',
            [
                "user" => $this->users->findById($id),
                "user_roles" => $this->userRole->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $this->users->update($user->id, $request->except(['_method', '_token']));
        return redirect()->route('users.show', ["user" => $user->id])->withSuccess(
            'User #' . $user->id . " was updated!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->users->destroy($id);
        return redirect()->route('users.index')->withSuccess('User #' . $id . " was deleted!");
    }
}
