<?php

namespace App\Http\Controllers;

use App\Models\GroupLearners;
use Illuminate\Http\Request;
use App\Services\GroupLearnersService;

class GroupLearnersController extends Controller
{
    public function __construct(GroupLearnersService $groupLearners)
    {
        $this->groupLearners = $groupLearners;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'groups_learners.index',
            ["groupsLearners" => $this->groupLearners->all()]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'groups_learners.create',
            [
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $groupLearners = $this->groupLearners->create($request->all());
        return redirect()->route('groupslearners.show', ["groupslearner"=>$groupLearners->id])->withSuccess(
            'Групу ' . $groupLearners->name . " було створено!"
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'groups_learners.show',
            ["groupslearner" => $this->groupLearners->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'groups_learners.edit',
            [
                "groupslearner" => $this->groupLearners->findById($id),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupLearners $groupslearner)
    {
        $this->groupLearners->update($groupslearner->id, $request->except(['_method', '_token']));
        return redirect()->route('groupslearners.show', ["groupslearner" => $groupslearner->id])->withSuccess(
            'Групу ' . $groupslearner->id . " було оновлено!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->groupLearners->destroy($id);
        return redirect()->route('groupslearners.index')->withSuccess('Групу #' . $id . " було видалено!");
    }
}
