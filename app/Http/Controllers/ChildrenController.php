<?php

namespace App\Http\Controllers;

use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Services\PersonService;
use App\Services\FamilyService;
use App\Services\ChildrenService;
use App\Http\Requests\PersonUpdateRequest;
use App\Models\Person;
use App\Http\Requests\PersonStoreRequest;
use Illuminate\Support\Facades\Auth;

class ChildrenController extends Controller
{
    public function __construct(ChildrenService $children, FamilyService $family)
    {
        $this->children = $children;
        $this->family = $family;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'children.index',
            ["children" => $this->children->getByFamily(Auth::user()),]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(
            'children.create',
            [
                "family" => $this->children->getByFamily(Auth::user())
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonStoreRequest $request)
    {
        $user = Auth::user();
        $person = $this->children->createChildrenWithFamily($request->all(),$user);
        return redirect()->route('children.show', ["child"=>$person->id])->withSuccess(
            (($person->sex=='male')? 'Малюка ' : 'Дівчинку '). $person->name . " було створено!"
        );
//            ->with('message',
//        ($person->sex='male')? 'Батька ' : 'Мати '. $person->name . " було створено!");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view(
            'children.show',
            ["child" => $this->children->findById($id)]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view(
            'children.edit',
            [
                "child" => $this->children->findById($id),
             //   "classes" => $this->family->all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonUpdateRequest $request, Person $child)
    {
        $this->children->update($child->id, $request->except(['_method', '_token']));
        return redirect()->route('children.show', ["child" => $child->id])->withSuccess(
            ($this->children->findById($child->id)->sex='male')? 'Малюка ' : 'Дівчинку ' . $child->id . " було оновлено!"
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->children->destroy($id);
        return redirect()->route('children.index')->withSuccess('Дані про дитину #' . $id . " було видалено!");
    }
}
