<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Services\UserRoleService;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRoleService $userRole)
    {
        parent::__construct();
        $this->userRole = $userRole;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->askValid('Enter name Admin:', "name", ['required','min:3', 'string', 'max:255']);
        $mobile_phone = $this->askValid('Enter mobile phone (10 digits):', "mobile_phone", ['required','digits:10', 'unique:users,mobile_phone']);
        $password = $this->askValid('Enter password:', "password", ['required','min:3']);
        $confirmPassword = $this->askValid('Enter confirm password:', "confirm_password", ['required','min:3', 'in:'.$password]);
        $Role = $this->userRole->getAdministrator();
        //$Role = UserRole::where('role', 'Administrator')->first();
        User::create(
            [
                "name" => $name,
                "mobile_phone" => $mobile_phone,
                'email_verified_at' => now(),
                'password' => Hash::make($password),
                'remember_token' => Str::random(10),
                'user_role_id' => $Role->id
            ]
        );
        $this->info('Admin user was successfully added!');
    }

    protected function askValid($question, $field, $rules)
    {
        $value = $this->ask($question);

        if ($message = $this->validateInput($rules, $field, $value)) {
            $this->error($message);

            return $this->askValid($question, $field, $rules);
        }

        return $value;
    }


    protected function validateInput($rules, $fieldName, $value)
    {
        $validator = Validator::make(
            [
                $fieldName => $value
            ],
            [
                $fieldName => $rules
            ]
        );

        return $validator->fails()
            ? $validator->errors()->first($fieldName)
            : null;
    }
}
