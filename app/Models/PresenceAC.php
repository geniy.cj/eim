<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PresenceAC extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'presence_ac';

    protected $fillable = [
        'student_person_id',
        'schedule_ac_id',
    ];
}
