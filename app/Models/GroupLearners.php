<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupLearners extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'groups_learners';

    protected $fillable = [
        'name',
        'price_education',
        'price_food',
    ];
}
