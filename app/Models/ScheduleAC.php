<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleAC extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'schedule_ac';

    protected $fillable = [
        'date',
        'teacher_person_id',
        'additional_classes_id',
    ];
}
