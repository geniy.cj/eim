<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presence extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'presence';

    protected $fillable = [
        'student_person_id',
        'date',
        'time_begin',
        'time_finish',
        'planned_presence',
        'actual_presence',
    ];
}
