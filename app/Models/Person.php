<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Family;
use Carbon\Carbon;

class Person extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'persons';

    protected $fillable = [
        'name',
        'surnmame',
        'middle_name',
        'birthday',
        'sex',
        'person_type',
        'family_id',
        'groups_learners_id',
    ];

    public function family()
    {
        return $this->belongsTo(Family::class, "family_id", "id");
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format("Y-m-d");
    }
}
