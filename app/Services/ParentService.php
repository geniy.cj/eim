<?php

namespace App\Services;

use App\Models\Family;
use App\Models\User;
use App\Services\BaseService;
use App\Services\FamilyService;
use App\Repositories\ParentRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ParentService extends BaseService
{
    public function __construct(ParentRepository $repo, FamilyService $serviceFamily, UserService $serviceUser)
    {
        $this->repo = $repo;
        $this->serviceFamily = $serviceFamily;
        $this->serviceUser = $serviceUser;
    }

    public function getByFamilyWithPagination(string $familyId): LengthAwarePaginator
    {
        return $this->repo->getByFamilyWithPagination($familyId);
    }

    public function search(string $name): Collection
    {
        return $this->repo->search($name);
    }

      /**
       * Create new record
       *
       * @param array $input
       * @return model
       */
    public function createParentWithFamily(array $data, User $user): Model
    {
        $data["family_id"] = $this->serviceFamily->create(["name"=>$data['surname']])->id;
        $data["person_type"] = 'parent';
        $data["name"] = $user->name;
        return $this->repo->create($data);
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function createWithFamily(array $data, User $user): Model
    {
        if (empty($user->person_id)) {
            $family = $this->serviceFamily->create(['name'=> $data['surname']]);
            $data["family_id"] = $family->id;
            $parent = $this->repo->create($data);
            $this->serviceUser->update($user->id, ['person_id'=> $parent->id]);
        } else {
            $parent = $this->repo->create($data);
        }
        return $parent;
    }

    /**
     * Get all By Family
     *
     * @return Collection
     */
    public function getByFamily(User $user): Collection
    {
        $familyId = !empty($user->person) ? $user->person->family_id : 0;
        return $this->repo->getByFamily($familyId);
    }

}
