<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }


    public function getAllNotRole($roleId): Collection
    {
        return $this->repo->getAllNotRole($roleId);
    }

    public function update(string $id, array $data): bool
    {
        return parent::update($id, $data);
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function create(array $data): Model
    {
//        if (!empty($data['person_id'])) {
//            $personId = $this->person->getIdByShortName($data['person_id']);
//            $data['person_id'] = $personId;
//        }
//        unset($data['person_id']);
        $data['password'] = Hash::make($data['password']);
        $data['type'] = 'user';
        return parent::create($data);
    }
}
