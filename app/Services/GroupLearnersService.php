<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\GroupLearnersRepository;

class GroupLearnersService extends BaseService
{
    public function __construct(GroupLearnersRepository $repo)
    {
        $this->repo = $repo;
    }
}
