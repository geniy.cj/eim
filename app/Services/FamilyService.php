<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\FamilyRepository;

class FamilyService extends BaseService
{
    public function __construct(FamilyRepository $repo)
    {
        $this->repo = $repo;
    }
}
