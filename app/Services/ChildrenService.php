<?php

namespace App\Services;

use App\Models\Person;
use App\Models\User;
use App\Services\ParentService;
use App\Services\BaseService;
use App\Repositories\ChildrenRepository;
use App\Services\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ChildrenService extends BaseService
{
    public function __construct(ChildrenRepository $repo, ParentService $serviceParent, UserService $serviceUser)
    {
        $this->repo = $repo;
        $this->serviceParent = $serviceParent;
        $this->serviceUser = $serviceUser;
    }

    public function getByFamilyWithPagination(string $familyId): LengthAwarePaginator
    {
        return $this->repo->getByFamilyWithPagination($familyId);
    }

    public function search(string $name): Collection
    {
        return $this->repo->search($name);
    }
    /**
     * Get all By Family
     *
     * @return Collection
     */
    public function getByFamily(User $user): Collection
    {
        $familyId = !empty($user->person) ? $user->person->family_id : 0;
        return $this->repo->getByFamily($familyId);
    }

    /**
     * Create new record
     *
     * @param array $input
     * @return model
     */
    public function createChildrenWithFamily(array $data,User $user): Model
    {
        if (empty($user->person_id)) {
            $person = $this->serviceParent->createParentWithFamily($data, $user);
            $this->serviceUser->update($user->id, ['person_id'=> $person->id]);
            $family_id = $person->family_id;
        } else {
            $family_id = $user->person->family->id;
        }
        $data["person_type"] = 'child';
        $data["family_id"] = $family_id;
        return $this->repo->create($data);
    }
}
