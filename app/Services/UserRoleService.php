<?php


namespace App\Services;

use App\Models\UserRole;
use App\Services\BaseService;
use App\Repositories\UserRoleRepository;
use Illuminate\Support\Collection;

class UserRoleService extends BaseService
{
    public function __construct(UserRoleRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getAdministrator(): UserRole
    {
        return $this->repo->getAdministrator();
    }

}
