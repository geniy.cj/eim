<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\PersonRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class PersonService extends BaseService
{
    public function __construct(PersonRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByFamilyWithPagination(string $familyId): LengthAwarePaginator
    {
        return $this->repo->getByFamilyWithPagination($familyId);
    }

    public function search(string $name): Collection
    {
        return $this->repo->search($name);
    }
}
