<?php

namespace App\Repositories;

use App\Models\GroupLearners;
use App\Repositories\BaseRepository;

class GroupLearnersRepository extends BaseRepository
{
    public function __construct(GroupLearners $model)
    {
        $this->model = $model;
    }
}
