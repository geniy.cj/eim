<?php


namespace App\Repositories;

use App\Models\UserRole;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class UserRoleRepository extends BaseRepository
{
    public function __construct(UserRole $model)
    {
        $this->model = $model;
    }

    public function getAdministrator(): UserRole
    {
        return $this->model->where('role', 'Administrator')->first();
    }

}
