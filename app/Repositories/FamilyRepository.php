<?php

namespace App\Repositories;

use App\Models\Family;
use App\Repositories\BaseRepository;

class FamilyRepository extends BaseRepository
{
    public function __construct(Family $model)
    {
        $this->model = $model;
    }
}
