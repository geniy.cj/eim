<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getAllNotRole($roleId): Collection
    {
        return $this->model->where("user_role_id", "<>", $roleId)->orWhereNull('user_role_id')->get();
    }
}
