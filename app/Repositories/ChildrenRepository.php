<?php

namespace App\Repositories;

use App\Models\Person;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ChildrenRepository extends BaseRepository
{
    public function __construct(Person $model)
    {
        $this->model = $model;
    }

    public function getByFamilyWithPagination(string $familyId): LengthAwarePaginator
    {
        return $this->model->where("family_id", $familyId)
            ->paginate($this->count);
    }

    public function search(string $name): Collection
    {
        return $this->model->where("name", 'like', '%' . $name . '%')->get();
    }

    public function getByFamily(string $familyId): Collection
    {
        return $this->model->where("family_id", $familyId)->where("person_type", "child")->get();
    }
}
