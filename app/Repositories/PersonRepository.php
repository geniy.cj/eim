<?php

namespace App\Repositories;

use App\Models\Person;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class PersonRepository extends BaseRepository
{
    public function __construct(Person $model)
    {
        $this->model = $model;
    }

    public function getByFamilyWithPagination(string $familyId): LengthAwarePaginator
    {
        return $this->model->where("family_id", $familyId)
            ->paginate($this->count);
    }

    public function search(string $name): Collection
    {
        return $this->model->where("name", 'like', '%' . $name.'%')->get();
    }
}
