<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChildrenController;
use App\Http\Controllers\UserManagementController;
use App\Http\Controllers\GroupLearnersController;
use App\Http\Controllers\PersonController;
use App\Http\Controllers\ParentController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome', ["message" => "Hello!"]);

Route::middleware(['auth'])->group(function () {
    Route::prefix('profile')->group(
        function () {
            Route::get('/', [UserController::class, 'profile'])->name('profile');
            Route::post('/update', [UserController::class, 'update'])->name('profile.update');
        }
    );

    Route::resource('persons', PersonController::class);
    Route::resource('children', ChildrenController::class);
    Route::resource('parents', ParentController::class);
});

Route::middleware(['auth', 'admin.check'])->group(function () {
    Route::prefix('management')->group(
        function () {
            Route::resource('users', UserManagementController::class);
        }
    );
});

Route::middleware(['auth', 'employee.check'])->group(function () {
    Route::prefix('management')->group(
        function () {
            Route::resource('groupslearners', GroupLearnersController::class);
        }
    );
});

Auth::routes();


require __DIR__.'/auth.php';
