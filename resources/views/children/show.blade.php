@extends('adminlte::page')

@section('content')
    {{ logger('Test') }}
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
            <div class="card">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid" src="{{ $child->image }}" alt="Person picture">
                    </div>
                    {{--#{{ $child->id }}--}}
                    <h3 class="profile-username text-center"> {{ $child->name }}</h3>

                    <p class="text-muted text-center">{{ $child->family->name }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Стать</b> <a class="float-right">{{ (($child->sex=='male')? 'Хлопчик ' : 'Дівчинка ') }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>День народження</b> <a class="float-right">{{ $child->birthday }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Creation Date</b> <a class="float-right">{{$child->created_at}}</a>
                        </li>
                    </ul>

                    <a href="{{route('children.edit', ["child" => $child->id])}}" class="btn btn-primary btn-block"><b>Змінити</b></a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@stop
