@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('children.update', ["child" => $child->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Інформація про дитину</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Ім'я</label>
                            <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ?? $child->name }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputSurname">Прізвище</label>
                            <input type="text" name="surname" id="inputSurname" class="form-control @error('surname') is-invalid @enderror" value="{{old('surname') ?? $child->surname }}">
                            @error('surname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputMiddleName">Ім'я по батькові</label>
                            <input type="text" name="middle_name" id="inputMiddleName" class="form-control @error('middle_name') is-invalid @enderror" value="{{old('middle_name') ?? $child->middle_name }}">
                            @error('middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputSex">Стать</label>
                            <select class="form-control" name="sex">
                                <option value="female" id="inputSex" class="form-control @error('sex') is-invalid @enderror" @if($child->sex === 'male') selected @endif}}>Хлопчик</option>
                                <option value="male"  id="inputSex" class="form-control @error('sex') is-invalid @enderror" @if($child->sex === 'female') selected @endif}}>Дівчинка</option>
                            </select>
                            @error('sex')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            {{ $child->sex}}
                        </div>

                        <div class="form-group">
                            <label for="birthday">День народження</label>
                            <input type="date" name="birthday" id="birthday" class="form-control @error('birthday') is-invalid @enderror" value="{{old('birthday')?? $child->birthday}}">
                            @error('birthday')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="inputStatus">Family</label>--}}
{{--                            <select name="family_id" class="form-control custom-select @error('family_id') is-invalid @enderror">--}}
{{--                                @foreach($families as $family)--}}
{{--                                    <option @if(old('family_id') && $family->id == old('family_id'))--}}
{{--                                            selected--}}
{{--                                            @elseif( empty(old('family_id')) && $family->id === $child->family->id)--}}
{{--                                            selected--}}
{{--                                            @endif--}}
{{--                                            value="{{ $family->id }}">{{ $family->name }}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            @error('family_id')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Save Changes" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop
