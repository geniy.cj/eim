@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="children" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Фото
                </th>
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">Ім'я
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Прізвище
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Ім'я по батькові
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Стать
                </th>

                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">День народження
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Дії
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($children as $child)
                <tr role="row">
                    <td>{{ $child->img }}</td>
                    <td>{{ $child->name }}</td>
                    <td>{{ $child->surname }}</td>
                    <td>{{ $child->middle_name }}</td>
                    <td>{{ (($child->sex=='male')? 'Хлопчик ' : 'Дівчинка ') }}</td>
                    <td>{{ $child->birthday }}</td>
                    <td>
                        <table>
                            <a class="btn btn-primary btn-sm" href="{{ route('children.show', ["child" => $child->id]) }}">
                                <i class="far fa-eye">
                                </i>
                            </a>&ensp;
                            <a class="btn btn-info btn-sm" href="{{ route('children.edit', ["child" => $child->id]) }}">
                                <i class="fas fa-pencil-alt">
                                </i>
                            </a>&ensp;
                            <form method="POST" action="{{ route('children.destroy', ["child" => $child->id]) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash">
                                    </i>
                                </button>
                            </form>
                        </table>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#children').DataTable({
                "paging":       true,
                "lengthChange": false,
                "searching":    true,
                "ordering":     true,
                "info":         true,
                "autoWidth":    false,
                "language": {
                    "infoFiltered":   "(Відфільтровано _MAX_ записів)",
                    "zeroRecords":    "Записів не знайдено",
                    "info": "Показано з _START_ по _END_ записів з _TOTAL_",
                    "lengthMenu": "Показувати _MENU_ записів на сторінці",
                    "infoEmpty": "Немає записів.",
                    "search": "Пошук:",
                    "paginate": {
                        "first": "Перша",
                        "previous": "&laquo; Назад",
                        "last": "Остання",
                        "next": "Далі &raquo;"
                    }
                },
            });
        });
    </script>
@stop
