@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
            <div class="card">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid" src="{{ $parent->image }}" alt="Person picture">
                    </div>

                    <h3 class="profile-username text-center">#{{ $parent->id }} {{ $parent->name }}</h3>

                    <p class="text-muted text-center">{{ $parent->family }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Стать</b> <a class="float-right">{{ $parent->sex }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>День народження</b> <a class="float-right">{{ $parent->birthday }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Creation Date</b> <a class="float-right">{{$parent->created_at}}</a>
                        </li>
                    </ul>

                    <a href="{{route('parents.edit', ["parent" => $parent->id])}}" class="btn btn-primary btn-block"><b>Edit</b></a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@stop
