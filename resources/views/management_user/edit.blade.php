@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('users.update', ["user" => $user->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Users Data</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ?? $user->name }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Mobile Phone</label>
                            <input type="string" name="mobile_phone" id="mobile_phone" class="form-control @error('mobile_phone') is-invalid @enderror" value="{{old('mobile_phone') ?? $user->mobile_phone }}">
                            @error('mobile_phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <script>
                            $('#mobile_phone').mask('(999)-999-99-99')
                        </script>
                        <div class="form-group">
                            <label for="price">Email</label>
                            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email') ?? $user->email }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputStatus">Role</label>
                            <select name="user_role_id" class="form-control custom-select @error('user_role') is-invalid @enderror">
                                <option value="" disabled>Select Role</option>
                                @foreach($user_roles as $user_role)
                                    <option @if(old('user_role') && $user_role->id == old('user_role'))
                                                selected
                                            @elseif( empty(old('user_role')) )
{{--                                           && $user_role->id === $user->UserRole->id--}}
                                                selected
                                            @endif
                                            value="{{ $user_role->id }}">{{ $user_role->role }}</option>
                                @endforeach
                            </select>
                            @error('user_role')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Save Changes" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop
