@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="users" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">Name
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Mobile phone
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Email
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Surname Person
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Role
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Actions
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr role="row">
                <td>{{ $user->name }}</td>
                <td>{{ $user->mobile_phone }}</td>
                <td>{{ $user->email }}</td>
                <td>@isset($user->person) {{ $user->person->surname }} @endisset</td>
                <td>@isset($user->UserRole) {{ $user->UserRole->role }} @endisset</td>
                <td>
                    <table>
                        <a class="btn btn-primary btn-sm" href="{{ route('users.show', ["user" => $user->id]) }}">
                            <i class="far fa-eye">
                            </i>
                        </a>&ensp;
                        <a class="btn btn-info btn-sm" href="{{ route('users.edit', ["user" => $user->id]) }}">
                            <i class="fas fa-pencil-alt">
                            </i>
                        </a>&ensp;
                        <form method="POST" action="{{ route('users.destroy', ["user" => $user->id]) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash">
                                </i>
                            </button>
                        </form>
                    </table>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#users').DataTable({
                "paging":       true,
                "lengthChange": false,
                "searching":    true,
                "ordering":     true,
                "info":         true,
                "autoWidth":    false,
                "language": {
                    "infoFiltered":   "(Відфільтровано _MAX_ записів)",
                    "zeroRecords":    "Записів не знайдено",
                    "info": "Показано з _START_ по _END_ записів з _TOTAL_",
                    "lengthMenu": "Показувати _MENU_ записів на сторінці",
                    "infoEmpty": "Немає записів.",
                    "search": "Пошук:",
                    "paginate": {
                        "first": "Перша",
                        "previous": "&laquo; Назад",
                        "last": "Остання",
                        "next": "Далі &raquo;"
                    }
                },
            });
        });
    </script>
@stop

