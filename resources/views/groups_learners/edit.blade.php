@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('groupslearners.update', ["groupslearner" => $groupslearner->id]) }}">
        @method('PUT')
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Users Data</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Назва</label>
                            <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" value="{{old('name') ?? $groupslearner->name }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Ціна навчання, грн</label>
                            <input type="number" name="price_education" id="price_education" class="form-control @error('price_education') is-invalid @enderror" value="{{old('price_education') ?? $groupslearner->price_education }}">
                            @error('price_education')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Ціна харчування, грн</label>
                            <input type="number" name="price_food" id="price_food" class="form-control @error('price_food') is-invalid @enderror" value="{{old('price_food') ?? $groupslearner->price_food }}">
                            @error('price_food')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Save Changes" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop
