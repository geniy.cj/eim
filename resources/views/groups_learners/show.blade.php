@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
        <div class="card">
            <div class="card-body box-profile">

                <h3 class="profile-username text-center">#{{ $groupslearner->id }} {{ $groupslearner->name }}</h3>

                <p class="text-muted text-center">{{ $groupslearner->price_education }}</p>

                <p class="text-muted text-center">{{ $groupslearner->price_food }}</p>

{{--                <ul class="list-group list-group-unbordered mb-3">--}}
{{--                    <li class="list-group-item">--}}
{{--                        <b>Person</b> <a class="float-right">@isset($user->person) {{ $user->person->surname }} @endisset</a>--}}
{{--                    </li>--}}
{{--                    <li class="list-group-item">--}}
{{--                        <b>Creation Date</b> <a class="float-right">{{$user->created_at}}</a>--}}
{{--                    </li>--}}
{{--                    <li class="list-group-item">--}}
{{--                        <b>Email Verified</b> <a class="float-right">{{$user->email_verified_at}}</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}

                <a href="{{route('groupslearners.edit', ["groupslearner" => $groupslearner->id])}}" class="btn btn-primary btn-block"><b>Змінити</b></a>
            </div>
        </div>
        </div>
    </div>
@stop

@section('js')
@stop
