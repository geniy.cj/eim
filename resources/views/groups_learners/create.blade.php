@extends('adminlte::page')

@section('content')
    <form method="POST" action="{{ route('groupslearners.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Додавання нової групи </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Назва групи</label>
                            <input type="text" name="name" id="inputName" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Ціна навчання, грн</label>
                            <input type="number" name="price_education" id="price_education" class="form-control @error('price_education') is-invalid @enderror" value="{{old('price_education') }}">
                            @error('price_education')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="price">Ціна харчування, грн</label>
                            <input type="number" name="price_food" id="price_food" class="form-control @error('price_food') is-invalid @enderror" value="{{old('price_food') }}">
                            @error('price_food')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Скасувати</a>
                <input type="submit" value="Create" class="btn btn-success float-right">
            </div>
        </div>
    </form>
@stop

@section('js')
@stop
