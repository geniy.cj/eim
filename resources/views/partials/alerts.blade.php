<div class="col-md-12">
    @if( Session::has( 'success' ))
        <div class="alert alert-success alert-dismissible">
            <h8><i class="icon fas fa-check"></i> Сповіщення!</h8>
            {{ Session::get( 'success' ) }}
        </div>
    @endif
</div>
