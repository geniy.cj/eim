@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <div class="col-md-12">
            <div class="card">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid" src="{{ $person->image }}" alt="Person picture">
                    </div>

                    <h3 class="profile-username text-center">#{{ $person->id }} {{ $person->name }}</h3>

                    <p class="text-muted text-center">{{ $person->family }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Стать</b> <a class="float-right">{{ $person->sex }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>День народження</b> <a class="float-right">{{ $person->birthday }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Creation Date</b> <a class="float-right">{{$person->created_at}}</a>
                        </li>
                    </ul>

                    <a href="{{route('persons.edit', ["person" => $person->id])}}" class="btn btn-primary btn-block"><b>Edit</b></a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
@stop
