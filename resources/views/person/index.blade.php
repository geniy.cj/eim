@extends('adminlte::page')

@section('content')
    <div class="row">
        @include('partials.alerts')
        <table id="persons" class="table table-bordered table-hover dataTable" role="grid"
               aria-describedby="example2_info">
            <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1"
                    aria-sort="ascending">Ім'я
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Прізвище
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Ім'я по батькові
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Стать
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Зображення
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">День народження
                </th>
                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Дії
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($persons as $person)
                <tr role="row">
                    <td>{{ $person->name }}</td>
                    <td>{{ $person->surname }}</td>
                    <td>{{ $person->middle_name }}</td>
                    <td>{{ $person->sex }}</td>
                    <td>{{ $person->img }}</td>
                    <td>{{ $person->birthday }}</td>
                    <td>
                        <table>
                            <a class="btn btn-primary btn-sm" href="{{ route('persons.show', ["person" => $person->id]) }}">
                                <i class="far fa-eye">
                                </i>
                            </a>&ensp;
                            <a class="btn btn-info btn-sm" href="{{ route('persons.edit', ["person" => $person->id]) }}">
                                <i class="fas fa-pencil-alt">
                                </i>
                            </a>&ensp;
                            <form method="POST" action="{{ route('persons.destroy', ["person" => $person->id]) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash">
                                    </i>
                                </button>
                            </form>
                        </table>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
        </table>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#persons').DataTable({
                "paging":       true,
                "lengthChange": false,
                "searching":    true,
                "ordering":     true,
                "info":         true,
                "autoWidth":    false,
                "language": {
                    "infoFiltered":   "(Відфільтровано _MAX_ записів)",
                    "zeroRecords":    "Записів не знайдено",
                    "info": "Показано з _START_ по _END_ записів з _TOTAL_",
                    "lengthMenu": "Показувати _MENU_ записів на сторінці",
                    "infoEmpty": "Немає записів.",
                    "search": "Пошук:",
                    "paginate": {
                        "first": "Перша",
                        "previous": "&laquo; Назад",
                        "last": "Остання",
                        "next": "Далі &raquo;"
                    }
                },
            });
        });
    </script>
@stop
