@push('scriptsbody')

@endpush
<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <img class="w-32 h-32 fill-current text-gray-500" src="{{ asset('storage/images/logo.png') }}" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors"/>

        <form method="POST" action="{{ route('register') }}">
        @csrf

        <!-- Name -->
            <div>

                <x-label for="name" :value="__('Ім\'я')"/>

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus/>
            </div>

            <!-- Mobile Phone -->
            <div class="mt-4">
                <x-label for="mobile_phone" :value="__('Номер мобільного телефону')"/>

                <x-input id="mobile_phone" class="block mt-1 w-full phone_number form-control" type="text" name="mobile_phone" :value="old('mobile_phone')" maxlength="15" required/>
            </div>
            <script>
                $('#mobile_phone').mask('(999)-999-99-99')
            </script>
{{--            <!-- Email Address -->--}}
{{--            <div class="mt-4">--}}
{{--                <x-label for="email" :value="__('Email')"/>--}}

{{--                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required/>--}}
{{--            </div>--}}


            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Пароль')"/>

                <x-input id="password" class="block mt-1 w-full"
                         type="password"
                         name="password"
                         required autocomplete="new-password"/>
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Підтвердьте пароль')"/>

                <x-input id="password_confirmation" class="block mt-1 w-full"
                         type="password"
                         name="password_confirmation" required/>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Вже зареєстровані?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Реєстрація') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>

